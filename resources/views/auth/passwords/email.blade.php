@extends('layouts.app', [
    'routeActive' => 'home',
])

@section('title', 'Esqueci minha senha')

@section('content')
    <section class="banner--section">
        <div class="banner--slider owl-carousel" data-owl-loop="false">
            <div class="banner--item" data-bg-img="{{ asset('img/banner-img/bg.jpg') }}" data-overlay="0.5">
                <div class="vc--parent">
                    <div class="vc--child">
                        <div class="container">
                            <div class="row row--md-vc">
                                <div class="col-md-7">
                                    <div class="banner--content pt--70 pb--80">
                                        <div class="title">
                                            <h1 class="h2 text-white">Bem-vindo ao Knives Marketplace</h1>
                                        </div>

                                        <div class="sub-title">
                                            <h2 class="h1 text-white">Conecte-se, compartilhe, venda</h2>
                                        </div>

                                        <div class="desc fs--16">
                                            <p>Descritivo da rede...</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-md-offset-1 col-xs-8 col-xs-offset-2 col-xxs-12">
                                    <div class="widget">
                                        @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                {{ session('status') }}
                                            </div>
                                            <br>
                                        @endif
                                        <h2 class="h4 fw--700 widget--title">Esqueci minha senha</h2>
                                        <div class="buddy-finder--widget">
                                            <form action="{{ route('password.email') }}" autocomplete="off" method="POST">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>
                                                                <span class="text-darker ff--primary fw--500">E-mail</span>
                                                                <input type="email" name="email" class="form-control form-sm" value="{{ old('email') }}">
                                                                @error('email')
                                                                    <span class="help-text text-danger" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary">Enviar</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <hr>
                                            <p class="text-center">
                                                <a href="{{ route('login') }}">Lembrei ou recuperei minha senha</a>
                                                <br>
                                                Ainda não possui acesso? <a href="{{ route('register') }}">Clique aqui</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection