@extends('layouts.app', [
    'routeActive' => 'home',
])

@section('title', 'Cadastro')

@section('content')
    <section class="banner--section">
        <div class="banner--slider owl-carousel" data-owl-loop="false">
            <div class="banner--item" data-bg-img="{{ asset('img/banner-img/bg.jpg') }}" data-overlay="0.5">
                <div class="vc--parent">
                    <div class="vc--child">
                        <div class="container">
                            <div class="row row--md-vc">
                                <div class="col-md-7">
                                    <div class="banner--content pt--70 pb--80">
                                        <div class="title">
                                            <h1 class="h2 text-white">Bem-vindo ao Knives Marketplace</h1>
                                        </div>

                                        <div class="sub-title">
                                            <h2 class="h1 text-white">Conecte-se, compartilhe, venda</h2>
                                        </div>

                                        <div class="desc fs--16">
                                            <p>Descritivo da rede...</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-md-offset-1 col-xs-8 col-xs-offset-2 col-xxs-12">
                                    <div class="widget">
                                        @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                {{ session('status') }}
                                            </div>
                                            <br>
                                        @endif
                                        <h2 class="h4 fw--700 widget--title">Cadastre-se</h2>
                                        <div class="buddy-finder--widget">
                                            <form action="{{ route('register') }}" method="POST" autocomplete="off">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>
                                                                <span class="text-darker ff--primary fw--500">Nome</span>
                                                                <input type="text" name="name" class="form-control form-sm" value="{{ old('name') }}">
                                                                @error('name')
                                                                    <span class="help-text text-danger" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>
                                                                <span class="text-darker ff--primary fw--500">E-mail</span>
                                                                <input type="email" name="email" class="form-control form-sm" value="{{ old('email') }}">
                                                                @error('email')
                                                                    <span class="help-text text-danger" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>
                                                                <span class="text-darker ff--primary fw--500">Senha</span>
                                                                <input type="password" name="password" class="form-control form-sm">
                                                                @error('password')
                                                                    <span class="help-text text-danger" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>
                                                                <span class="text-darker ff--primary fw--500">Confirmação de senha</span>
                                                                <input type="password" name="password_confirmation" class="form-control form-sm">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary">Entrar</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <hr>
                                            <p class="text-center">
                                                Já possui acesso? <a href="{{ route('login') }}">Clique aqui</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
