<!DOCTYPE html>
<html dir="ltr" lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | Knives Marketplace</title>
    <meta name="author" content="Jefferson Camargo">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="favicon.png" type="image/png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700%7CRoboto:300,400,400i,500,700">
    <link rel="stylesheet" href="{{ asset('css/plugins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive-style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/colors/color-5.css" id="changeColorScheme') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div id="preloader">
        <div class="preloader--inner"></div>
    </div>
    <div class="wrapper">
        <header class="header--section style--3">
            <div class="header--topbar bg-dark">
                <div class="container">
                    <ul class="header--topbar-links nav ff--primary no--stripes float--left">
                        <li><a href="#">{{ __("Contato") }}</a></li>
                    </ul>
                    <ul class="header--topbar-links nav ff--primary float--right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span>Br</span>
                                <i class="fa fa-caret-down"></i>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="active"><a href="#">En</a></li>
                                <li><a href="#">Br</a></li>
                            </ul>
                        </li>
                        @if(Auth::user())
                            <li>
                                <a href="cart.html" title="Carrinho" data-toggle="tooltip" data-placement="bottom">
                                    <i class="fa fa-shopping-basket"></i>
                                    {{-- Contador de produtos no carrinho --}}
                                    <span class="badge">0</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="btn-link">
                                    <i class="fa fa-user-o"></i>
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="header--navbar navbar bg-black" data-trigger="sticky">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle style--3 collapsed" data-toggle="collapse" data-target="#headerNav">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="header--navbar-logo navbar-brand">
                            <a href="{{ route('home') }}">
                                <img src="{{ asset('img/logo-white.png') }}" alt="">
                            </a>
                        </div>
                    </div>
                    @if(Auth::user())
                        <div class="header--search style--2 float--right" data-form="validate">
                            <form action="">
                                <input type="text" name="search" placeholder="Procurar por..." class="form-control" required>
                                <button type="submit" class="btn-link"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    @endif
                    <div id="headerNav" class="navbar-collapse collapse float--right">
                        <ul class="header--nav-links style--3 nav ff--primary">
                            <li class="{{ isset($routeActive) && $routeActive == 'home' ? 'active' : '' }}">
                                <a href="#">
                                    <span>Home</span>
                                    {{-- <i class="fa fa-caret-down"></i> --}}
                                </a>
                            </li>
                            @if(Auth::user())
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span>Comunidade</span>
                                        <i class="fa fa-caret-down"></i>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li><a href="activity.html"><span>Feed</span></a></li>
                                        <li><a href="members.html"><span>Membros</span></a></li>
                                        <li><a href="groups.html"><span>Grupos</span></a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span>Loja</span>
                                        <i class="fa fa-caret-down"></i>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li><a href="products.html">Produtos</a></li>
                                        <li><a href="cart.html">Carrinho</a></li>
                                    </ul>
                                </li>
                            @endif
                            <li><a href="contact.html"><span>Contato</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        @yield('content')

        <footer class="footer--section">
            <div class="footer--widgets pt--70 pb--20 bg-lightdark" data-bg-img="img/footer-img/footer-widgets-bg.png">
                <div class="container">
                    <div class="row AdjustRow">
                        <div class="col-md-4 col-xs-6 col-xxs-12 pb--60">
                            <div class="widget">
                                <h2 class="h4 fw--700 widget--title">Sobre nós</h2>

                                <div class="text--widget">
                                    <p>Sua rede de cutelaria...</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-6 col-xxs-12 pb--60">
                            <div class="widget">
                                <h2 class="h4 fw--700 widget--title">Navegue</h2>
                                <div class="links--widget">
                                    <ul class="nav">
                                        @if(!Auth::user())
                                            <li><a href="#">Registre-se</a></li>
                                            <li><a href="#">Login</a></li>
                                        @else
                                            <li><a href="#">Feed</a></li>
                                            <li><a href="#">Membros</a></li>
                                            <li><a href="#">Grupos</a></li>
                                            <li><a href="#">Loja</a></li>
                                        @endif
                                        <li><a href="#">Contato</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-6 col-xxs-12 pb--60">
                            <div class="widget">
                                <h2 class="h4 fw--700 widget--title">Fique por dentro das novidades</h2>
                                <div class="newsletter--widget style--2" data-form="validate">
                                    <p>Inscreva-se em nossa newsletter.</p>

                                    <form action="" method="post" name="mc-embedded-subscribe-form" target="_blank">
                                        <div class="input-group">
                                            <input type="email" name="email" placeholder="Informe seu e-mail" class="form-control" autocomplete="off" required>

                                            <div class="input-group-btn">
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-send-o"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer--copyright pt--30 pb--30 bg-darkest">
                <div class="container">
                    <div class="text fw--500 fs--14 float--left">
                        <p>Copyright &copy; Knives Marketplace. Todos os direitos reservados.</p>
                        <p>Desenvolvido com <i class="fa fa-heart text-primary"></i> por <a href="https://jeffersoncamargo.com" target="_blank">Jefferson Camargo</a></p>
                    </div>
                    <div class="social float--right">
                        <ul class="nav">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <div id="backToTop">
        <a href="#" class="btn"><i class="fa fa-caret-up"></i></a>
    </div>
    <script src="{{ asset('js/plugins.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
</body>
</html>